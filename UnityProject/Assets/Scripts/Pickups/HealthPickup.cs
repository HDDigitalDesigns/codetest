﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    // Public Variables that can be changed in editor
    public int healthResupply = 50;

    // When an object collides and has the TankHealth Component
    // Add health to that tank
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<TankHealth>())
        {
            var health = collision.transform.GetComponent<TankHealth>();
            health.AddHealth(healthResupply);
            GameObject.Destroy(transform.gameObject);
        }
    }
}

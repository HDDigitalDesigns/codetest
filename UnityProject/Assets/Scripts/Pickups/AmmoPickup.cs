﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickup : MonoBehaviour
{
    // Puvlic variables that can be changed in editor
    public int shellResupply = 5;
    public int bulletResupply = 50;

    // When something collides and has TankAmmo Component
    // Add the ammo to that tank.
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.GetComponent<TankAmmo>())
        {
            var ammo = collision.transform.GetComponent<TankAmmo>();
            ammo.AddAmmo(shellResupply, bulletResupply);
            GameObject.Destroy(transform.gameObject);
        }
    }
}

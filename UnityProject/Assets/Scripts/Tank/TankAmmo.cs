﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankAmmo : MonoBehaviour
{
    // Added Ammo Types
    public enum AmmoType
    {
        Shell,
        Bullet
    };
    public AmmoType ammoType;
    public List<int> maxAmmoCount = new List<int>();        // Stores the max ammo count
    public List<bool> ammoDepleted = new List<bool>();      // Stores which ammo is depleted

    public float bulletRange = 100f;                        // Range for the raycast
    public float bulletDamage = 10f;                        // Amount of damage that can be done by the bullets

    private List<int> ammoCount = new List<int>();          // Stores the current amount of ammo

    public List<Text> ammoText = new List<Text>();          // Reference to the UI for the ammo text
    public List<Image> ammoImage = new List<Image>();       // Reference to the UI for the ammo image

    private bool isHuman = false;
    

    public void Start()
    {
        // Find out if the Tank brain is Human or Bot
        if (GetComponent<TankThinker>().brain.name == "Human 1")
            isHuman = true;
        else
            isHuman = false;

        // Initialize the different amount of ammos
        ammoCount.Add(maxAmmoCount[0]);
        ammoCount.Add(maxAmmoCount[1]);
        ammoDepleted.Add(false);
        ammoDepleted.Add(false);
        ammoDepleted[0] = false;
        ammoDepleted[1] = false;

        // Grab the UI Components from the scene
        ammoText.Add(GameObject.Find("Ammo_Shell_Count").GetComponent<Text>());
        ammoText.Add(GameObject.Find("Ammo_Bullet_Count").GetComponent<Text>());
        // Update the UI Components
        UpdateAmmoCountUI();

        // Grab the UI Components from the scene
        ammoImage.Add(GameObject.Find("Ammo_Shell").GetComponent<Image>());
        ammoImage.Add(GameObject.Find("Ammo_Bullet").GetComponent<Image>());

        // Update the UI Components
        ammoType = AmmoType.Shell;
        ammoImage[0].color = new Color32(255, 255, 255, 255);
        ammoImage[1].color = new Color32(255, 255, 255, 255);
    }
    


    public void DepleteAmmo()
    {
        // Deplete the correct ammo type if it is above zero
        if (ammoCount[(int)ammoType] > 0)
        {
            ammoCount[(int)ammoType]--;
        }
        // Update the depleted variable to disable firing of this ammo
        if (ammoCount[(int)ammoType] <= 0)
        {
            ammoDepleted[(int)ammoType] = true;
        }
        // Update the UI Components
        UpdateAmmoCountUI();
    }
    public void SwitchAmmo()
    {
        
        if (ammoType == AmmoType.Shell)
        {
            // Switch the ammo type to bullet and update the UI component
            ammoType = AmmoType.Bullet;
            ammoImage[0].color = new Color32(255, 255, 255, 90);
            ammoImage[1].color = new Color32(255, 255, 255, 255);
        }
        else if (ammoType == AmmoType.Bullet)
        {
            // Switch the ammo type to shell and update the UI component
            ammoType = AmmoType.Shell;
            ammoImage[0].color = new Color32(255, 255, 255, 255);
            ammoImage[1].color = new Color32(255, 255, 255, 90);
        }
    }

    private void UpdateAmmoCountUI()
    {
        if (isHuman) {
            // Updates the UI Text displaying the ammo count for each ammo type
            ammoText[0].text = ammoCount[0].ToString();
            ammoText[1].text = ammoCount[1].ToString();
        }
    }

    public void AddAmmo(int shell, int bullet)
    {
        // Adds ammo to the ammo count when colliding with pickup
        ammoCount[0] += shell;
        ammoCount[1] += bullet;
        // Update the UI Components
        UpdateAmmoCountUI();
    }
}

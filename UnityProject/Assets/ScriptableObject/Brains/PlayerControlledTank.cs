﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName="Brains/Player Controlled")]
public class PlayerControlledTank : TankBrain
{

	public int PlayerNumber;
	private string m_MovementAxisName;
	private string m_TurnAxisName;
	private string m_FireButton;

	private string switchButton;

	public void OnEnable()
	{
		m_MovementAxisName = "Vertical" + PlayerNumber;
		m_TurnAxisName = "Horizontal" + PlayerNumber;
		m_FireButton = "Fire" + PlayerNumber;
		switchButton = "Switch" + PlayerNumber;
	}

	public override void Think(TankThinker tank)
	{
		var movement = tank.GetComponent<TankMovement>();

		movement.Steer(Input.GetAxis(m_MovementAxisName), Input.GetAxis(m_TurnAxisName));

		var shooting = tank.GetComponent<TankShooting>();
		var ammo = tank.GetComponent<TankAmmo>();

		if (Input.GetButtonUp(switchButton))
        {
			ammo.SwitchAmmo();
        }

		// Check the ammo type
		if (ammo.ammoType == TankAmmo.AmmoType.Shell)
        {
			// Check the ammo is not depleted
            if (!ammo.ammoDepleted[(int)TankAmmo.AmmoType.Shell])
            {
				// Fire the shell
                if (Input.GetButton(m_FireButton))
                {
                    shooting.BeginChargingShot();
                }
                else
                {
                    shooting.FireChargedShot();
                } 
            }
		}
		// Check the ammo type
		if (ammo.ammoType == TankAmmo.AmmoType.Bullet)
        {
			// Check the ammo is not depleted
			if (!ammo.ammoDepleted[(int)TankAmmo.AmmoType.Bullet])
            {
				// Fire the bullet
				if (Input.GetButtonDown(m_FireButton))
				{
					shooting.FireBullet();
					shooting.isFiring = true;
				}
				else
					shooting.isFiring = false;
            }
        }
		
	}
}
